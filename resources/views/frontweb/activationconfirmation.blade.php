@extends('web_layout')

@section('maincontent')
        <!--=== Content Part ===-->
        <div class="container content-sm">
                <!-- Service Blocks -->
                <div class="row margin-bottom-30">
                        <div class="col-md-12">
                                <div class="service">
                                        <i class="fa fa-compress service-icon"></i>
                                        <div class="desc">
                                                <h4>Your account is now active</h4>
                                                <br/>
                                                <p>Dear <strong>{{$user->first_name}} {{$user->last_name}}, </strong>
                                                    Your account has been activated. You may now <a href="{{url('web/sign-in')}}">login</a> to the site using your chosen username.
                                                </p>
                                        </div>
                                </div>
                        </div>
                </div>
        </div><!--/container-->
        <!-- End Content Part -->
@stop
@section('pagejs')
<script type="text/javascript" src="{{ asset('public/assets/plugins/counter/waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/plugins/counter/jquery.counterup.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/app.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/plugins/owl-carousel.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/plugins/style-switcher.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/plugins/parallax-slider.js')}}"></script>
<script type="text/javascript">
        jQuery(document).ready(function() {
                App.init();
                OwlCarousel.initOwlCarousel();
                App.initCounter();
                StyleSwitcher.initStyleSwitcher();
                ParallaxSlider.initParallaxSlider();
        });
</script>
@stop